package ru.pisarev.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pisarev.tm.model.UserGraph;

public interface IUserRepository extends JpaRepository<UserGraph, String> {

    UserGraph findByLogin(final String login);

    UserGraph findByEmail(final String email);

    void deleteByLogin(final String login);

}
