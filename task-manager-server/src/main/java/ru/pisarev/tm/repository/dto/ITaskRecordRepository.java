package ru.pisarev.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.pisarev.tm.dto.TaskRecord;

import java.util.List;

public interface ITaskRecordRepository extends JpaRepository<TaskRecord, String> {

    @Query("SELECT e FROM TaskRecord e WHERE e.userId = :userId AND e.projectId = :projectId")
    List<TaskRecord> findAllTaskByProjectId(final String userId, final String projectId);

    @Modifying
    @Query("DELETE FROM TaskRecord e WHERE e.userId = :userId AND e.projectId = :projectId")
    void removeAllTaskByProjectId(final String userId, final String projectId);

    @Modifying
    @Query("UPDATE TaskRecord e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :id")
    void bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    @Modifying
    @Query("UPDATE TaskRecord e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
    void unbindTaskById(final String userId, final String id);

    TaskRecord findByUserIdAndId(final String userId, final String id);

    void deleteByUserId(final String userId);

    void deleteByUserIdAndId(final String userId, final String id);

    List<TaskRecord> findAllByUserId(final String userId);

    TaskRecord findByUserIdAndName(final String userId, final String name);

    @Query("SELECT e FROM TaskRecord e WHERE e.userId = :userId")
    TaskRecord findByIndexAndUserId(final String userId, final int index);

    void deleteByUserIdAndName(final String userId, final String name);

}
