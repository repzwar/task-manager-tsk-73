package ru.pisarev.tm.service.dto;

import ru.pisarev.tm.api.IRecordService;
import ru.pisarev.tm.dto.AbstractRecord;
import ru.pisarev.tm.service.AbstractService;

public abstract class AbstractRecordService<E extends AbstractRecord> extends AbstractService implements IRecordService<E> {

}
