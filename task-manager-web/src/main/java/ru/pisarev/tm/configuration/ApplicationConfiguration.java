package ru.pisarev.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.pisarev.tm")
public class ApplicationConfiguration {

}
