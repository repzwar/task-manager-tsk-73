package ru.pisarev.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    @NotNull
    public ProjectNotFoundException() {
        super("Error. ProjectRecord not found.");
    }

}
