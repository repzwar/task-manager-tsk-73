package ru.pisarev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.ProjectRecord;
import ru.pisarev.tm.endpoint.ProjectEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.listener.ProjectAbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class ProjectShowByIndexListener extends ProjectAbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-show-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    @EventListener(condition = "@projectShowByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectRecord project = projectEndpoint.findProjectByIndex(getSession(), index);
        if (project == null) throw new ProjectNotFoundException();
        show(project);
    }

}
